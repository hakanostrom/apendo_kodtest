const fs = require('fs');

// ========== 1. Läs in textfil ==========
let text = fs.readFileSync('assets/textfil.text', 'UTF-8');

// ========== 2. Sök ordet 'apendo' ==========
let regex = new RegExp('\\sapendo[\\s\\.\\?\\!]','gi');// /\sapendo[\s\.\?\!]/gi;
let finding = text.match(regex);

console.log(`Ordet 'apendo' hittades ${finding.length} gånger i texten`);

// ========== 3. Läs in och sök ord ==========
const prompt = require('prompt');
var colors = require("colors/safe");
prompt.message = "";
prompt.start();

let schema = [{
    name: 'keyword',
    description: colors.magenta("Ange sökord"),
    pattern: /^[a-zåäö]+$/i,
    default: 'erat'
},{
    name: 'casesensitive',
    description: colors.magenta("Skiflägeskänslig"),
    type: 'boolean',
    default: 'true'
}];

prompt.get(schema, function (err, input) {

    regex = new RegExp(`\\s${input.keyword}[\\s\\.\\?\\!]`,'g' + (input.casesensitive?'':'i'));
    finding = text.match(regex);

    console.log(`Ordet '${input.keyword}' hittades ${finding?finding.length:'0'} gånger i texten.`);
    
    del4();
});

// ========== 4. meningar och stycken ==========
function del4(){
    regex = new RegExp('\\sapendo[\\s\\.\\?\\!]','gi');

    let meningar = text.split(/[\.\!\?]+\s*|\n+\s*/g);
    let apendomeningar = meningar.filter(text => !!text.match(regex));
    let stycken = text.split(/[\n]+/g).filter(text => !!text.match(regex));

    console.log(`Ordet 'apendo' hittades i ${apendomeningar.length} meningar och ${stycken.length} stycken.`);


    // ========== 5. saknade meningar ==========

    console.log(`Ordet 'apendo' saknas i ${meningar.length - apendomeningar.length} (av totalt ${meningar.length}) meningar.`);


    // ========== 6. modifiera meningar ==========

    schema = [{
        name: 'insertafter',
        description: colors.magenta("Infoga det magiska ordet efter"),
        type: 'number',
        default: 4
    }];

    prompt.get(schema, function (err, input) {
        const plats = input.insertafter;
        regex = new RegExp('((\\w+[ |,]+){'+plats+'})');
        let moddadarray = apendomeningar.map(function(mening){

            if(mening.search(regex)>-1)
                return mening.replace(regex,'$1SUNDSVALL ');
            else
                return mening + " SUNDSVALL";

        
        });

        console.log(moddadarray);

        del7();
    });
}

// ========== 7. Swap-a-doodle-do ==========

function del7(){

    let schema = [{
        name: 'y',
        description: colors.magenta("Y"),
        type: 'string',
        default: 'y'
    },{
        name: 'z',
        description: colors.magenta("Z"),
        type: 'string',
        default: 'z'
    }];

    prompt.get(schema, function (err, input) {

        regex = /[\,\.\!\?\s]+/gi;

        let result = text.split(regex).filter(ord => ord.toLowerCase().includes(input.y.toLowerCase()));
        let resultSave = result;

        // ========== 7.1 ==========
        console.log('== 7.1 ==');
        for(let i=0;i<result.length;i++)
            console.log( (i+1) + '. ' + result[i] );

        // ========== 7.2 ==========
        console.log('== 7.2 ==');
        result = result.map(ord => ord.replace(input.y.toUpperCase(), input.z.toUpperCase()).replace(input.y.toLowerCase(), input.z.toLowerCase()) );
        result.sort();
        for(let i=0;i<result.length;i++)
            console.log( (i+1) + '. ' + result[i] );

        // ========== 7.3 ==========
        console.log('== 7.3 ==');
        result = resultSave.join() ;
        console.log( result );
        console.log( result.split('').reverse().join('') );
    });

}
